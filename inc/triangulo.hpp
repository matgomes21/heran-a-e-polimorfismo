#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"

using namespace std;

class Triangulo : public FormaGeometrica{
public:
  Triangulo();
  Triangulo(float base);
  Triangulo(float base, float altura);
  ~Triangulo();
  float calcula_area();
  float calcula_perimetro();
};

#endif
