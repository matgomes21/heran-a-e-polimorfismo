#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"

using namespace std;

class Hexagono : public FormaGeometrica{
public:
  Hexagono();
  Hexagono(float base);
  ~Hexagono();
  float calcula_area();
  float calcula_perimetro();
};

#endif
