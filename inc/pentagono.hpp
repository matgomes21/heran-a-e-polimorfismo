#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica{
private:
  float apotema;
public:
  Pentagono();
  Pentagono(float base);
  Pentagono(float base, float apotema);
  ~Pentagono();
  void set_apotema(float apotema);
  float get_apotema();
  float calcula_area();
  float calcula_perimetro();
};

#endif
