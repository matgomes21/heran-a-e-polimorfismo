#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

using namespace std;

class Paralelogramo : public FormaGeometrica{
private:
  float lado;
public:
  Paralelogramo();
  Paralelogramo(float base);
  Paralelogramo(float base, float altura);
  Paralelogramo(float base, float altura, float lado);
  ~Paralelogramo();
  void set_lado(float lado);
  float get_lado();
  float calcula_area();
  float calcula_perimetro();
};

#endif
