#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "triangulo.hpp"
#include "hexagono.hpp"
#include "pentagono.hpp"
#include "paralelogramo.hpp"
#include <iostream>
#include <vector>

using namespace std;

int main(){
    FormaGeometrica *figura0 = new FormaGeometrica(4.0,2.0);
    Quadrado *figura1 = new Quadrado(7.0,7.0);
    Triangulo *figura2 = new Triangulo(8.0,4.0);
    Circulo *figura3 = new Circulo(6.0);
    Paralelogramo *figura4 = new Paralelogramo(5.0,4.0,6.0);
    Pentagono *figura5 = new Pentagono(10.0,8.0);
    Hexagono *figura6 = new Hexagono(6.0);

    vector <FormaGeometrica*> figuras;
    figuras.push_back(figura0);
    figuras.push_back(figura1);
    figuras.push_back(figura2);
    figuras.push_back(figura3);
    figuras.push_back(figura4);
    figuras.push_back(figura5);
    figuras.push_back(figura6);

    for(int i=0;i<=6;i++){
      cout << "Tipo: " << figuras[i]->get_tipo() << endl;
      cout << "Área: " << figuras[i]->calcula_area() << endl;
      cout << "Perímetro: " << figuras[i]->calcula_perimetro() << endl;
      cout << endl;
    }

}
