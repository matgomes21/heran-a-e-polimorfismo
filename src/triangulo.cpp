#include "triangulo.hpp"
#include<iostream>
#include<math.h>

using namespace std;

Triangulo::Triangulo(){
  set_tipo("Triângulo");
  set_base(4.0);
  set_altura(2.0);
}
Triangulo::Triangulo(float base){
  set_tipo("Triângulo");
  set_base(base);
  set_altura(base);
}
Triangulo::Triangulo(float base,float altura){
  set_tipo("Triângulo");
  set_base(base);
  set_altura(altura);
}
float Triangulo::calcula_area(){
  return (get_base()*get_altura())/2.0;
}
float Triangulo::calcula_perimetro(){
  return sqrt((get_base()*get_base()) + (get_altura()*get_altura()))+get_base()+get_altura();
}
