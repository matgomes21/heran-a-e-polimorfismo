#include "circulo.hpp"
#include <iostream>


using namespace std;

void Circulo::set_raio(float raio){
  this->raio = raio;
}
float Circulo::get_raio(){
  return raio;
}
Circulo::Circulo(){
  set_tipo("Círculo");
  set_raio(5.0);
}
Circulo::Circulo(float raio){
  set_tipo("Círculo");
  set_raio(raio);
}
float Circulo::calcula_area(){
  return get_raio()*get_raio()*3.14159265;
}
float Circulo::calcula_perimetro(){
  return 2.0*get_raio()*3.14159265;
}
