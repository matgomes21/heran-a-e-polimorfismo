#include "pentagono.hpp"

using namespace std;

Pentagono::Pentagono(){
  set_tipo("Pentágono");
  set_base(5.0);
  set_apotema(4.0);
}
Pentagono::Pentagono(float base){
  set_tipo("Pentágono");
  set_base(base);
  set_apotema(base);
}
Pentagono::Pentagono(float base, float apotema){
  set_tipo("Pentágono");
  set_base(base);
  set_apotema(apotema);
}
void Pentagono::set_apotema(float apotema){
  this->apotema = apotema;
}
float Pentagono::get_apotema(){
  return apotema;
}
float Pentagono::calcula_perimetro(){
  return 5*get_base();
}
float Pentagono::calcula_area(){
  return Pentagono::calcula_perimetro() * apotema / 2.0;
}
