#include "quadrado.hpp"
#include <iostream>

using namespace std;

Quadrado::Quadrado(){
    set_tipo("Quadrado");
    set_base(5.0);
    set_altura(5.0);
}
Quadrado::Quadrado(float base){
    set_tipo("Quadrado");
    set_base(base);
    set_altura(base);
}
Quadrado::Quadrado(float base,float altura){
    if(base!=altura)
      throw(1);
    set_tipo("Quadrado");
    set_base(base);
    set_altura(base);
}
float Quadrado::calcula_area(){
    return get_base()*get_altura();
}
float Quadrado::calcula_perimetro(){
    return 2*get_base() + 2*get_altura();
}
