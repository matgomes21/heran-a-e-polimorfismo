#include "hexagono.hpp"
#include <math.h>

using namespace std;

Hexagono::Hexagono(){
  set_tipo("Hexágono");
  set_base(5.0);
}
Hexagono::Hexagono(float base){
  set_tipo("Hexágono");
  set_base(base);
}
float Hexagono::calcula_area(){
  return get_base()*get_base()*3.0*sqrt(3)/2.0;
}
float Hexagono::calcula_perimetro(){
  return 6*get_base();
}
