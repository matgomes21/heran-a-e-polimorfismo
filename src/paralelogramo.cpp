#include "paralelogramo.hpp"

using namespace std;

Paralelogramo::Paralelogramo(){
  set_tipo("Paralelogramo");
  set_base(2.0);
  set_altura(3.0);
  set_lado(4.0);
}
Paralelogramo::Paralelogramo(float base){
  set_tipo("Paralelogramo");
  set_base(base);
  set_altura(base);
  set_lado(base);
}
Paralelogramo::Paralelogramo(float base,float altura){
  set_tipo("Paralelogramo");
  set_base(base);
  set_altura(altura);
  set_lado(base);
}
Paralelogramo::Paralelogramo(float base,float altura,float lado){
  set_tipo("Paralelogramo");
  set_base(base);
  set_altura(altura);
  set_lado(lado);
}
void Paralelogramo::set_lado(float lado){
  this->lado = lado;
}
float Paralelogramo::get_lado(){
  return lado;
}
float Paralelogramo::calcula_area(){
  return get_base()*get_altura();
}
float Paralelogramo::calcula_perimetro(){
  return 2*get_base() + 2*get_lado();
}
